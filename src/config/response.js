const successCode = (res, data, messenge) => {
  res.status(200).json({
    messenge,
    content: data,
  });
};

const failCode = (res, data, messenge) => {
  res.status(400).json({
    messenge,
    content: data,
  });
};

const errorCode = (res, messenge) => {
  res.status(500).json({
    messenge,
  });
};

module.exports = {
  successCode,
  failCode,
  errorCode,
};
