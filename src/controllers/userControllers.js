const { Sequelize } = require("sequelize");
const { successCode, failCode, errorCode } = require("../config/response");

const initModels = require("../models/init-models");
const sequelize = require("../models/index");
const model = initModels(sequelize);

const Op = Sequelize.Op;

const getAllUser = async (req, res) => {
  try {
    let data = await model.user.findAll({
      include: ["orders", "like_res", "rate_res"],
    });
    successCode(res, data, "Trả dữ liệu thành công");
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const getUser = async (req, res) => {
  try {
    let id = req.params.id;
    let data = await model.user.findOne({
      where: {
        user_id: id,
      },
      include: ["orders", "like_res", "rate_res"],
    });
    successCode(res, data, "Trả dữ liệu thành công");
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const searchUser = async (req, res) => {
  try {
    let keyword = req.query.keyword;
    let data = await model.user.findAll({
      where: {
        full_name: {
          [Op.like]: `%${keyword}%`,
        },
      },
      include: ["orders", "like_res", "rate_res"],
    });
    successCode(res, data, "Trả dữ liệu thành công");
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const createUser = async (req, res) => {
  try {
    model.user.create(req.body);
    successCode(res, req.body, "Tạo mới thành công");
  } catch (err) {
    errorCode(res, "Lỗi BE");
  }
};

const updateUser = async (req, res) => {
  try {
    let id = req.params.id;
    model.user.update(req.body, {
      where: {
        user_id: id,
      },
    });
    successCode(res, req.body, "Cập nhật thành công");
  } catch (err) {
    errorCode(res, "Lỗi BE");
  }
};

const deleteUser = async (req, res) => {
  try {
    let id = req.params.id;
    model.user.destroy({
      where: {
        user_id: id,
      },
    });
    successCode(res, null, "Xóa thành công");
  } catch (err) {
    errorCode(res, "Lỗi BE");
  }
};

module.exports = {
  getAllUser,
  createUser,
  updateUser,
  getUser,
  searchUser,
  deleteUser,
};
