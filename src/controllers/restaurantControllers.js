const initModels = require("../models/init-models");
const sequelize = require("../models/index");

const model = initModels(sequelize);
const { successCode, failCode, errorCode } = require("../config/response");

const getRestaurant = async (req, res) => {
  try {
    let id = req.params.id;
    let data = await model.restaurant.findAll({
      where: {
        res_id: id,
      },
      include: ["like_res", "rate_res"],
    });
    successCode(res, data, "Trả dữ liệu thành công");
  } catch (err) {
    errorCode(res, "Lỗi BE");
  }
};

const likeRestaurant = async (req, res) => {
  try {
    model.like_res.create({ ...req.body, date_like: new Date() });
    successCode(res, null, "Like thành công");
  } catch (err) {
    errorCode(res, "Lỗi BE");
  }
};

const unlikeRestaurant = async (req, res) => {
  let { user_id, res_id } = req.body;
  try {
    model.like_res.destroy({
      where: {
        user_id: user_id,
        res_id: res_id,
      },
    });
    successCode(res, null, "Unlike thành công");
  } catch (err) {
    errorCode(res, "Lỗi BE");
  }
};

const rateRestaurant = async (req, res) => {
  try {
    model.rate_res.create({ ...req.body, date_rate: new Date() });
    successCode(res, null, "Đánh giá thành công");
  } catch (err) {
    errorCode(res, null, "Lỗi BE");
  }
};

module.exports = {
  getRestaurant,
  likeRestaurant,
  unlikeRestaurant,
  rateRestaurant,
};
