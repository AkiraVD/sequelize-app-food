const { Sequelize } = require("sequelize");
const { successCode, failCode, errorCode } = require("../config/response");

const initModels = require("../models/init-models");
const sequelize = require("../models/index");
const model = initModels(sequelize);

const Op = Sequelize.Op;

const getAllFood = async (req, res) => {
  try {
    let data = await model.food.findAll({
      include: ["type"],
    });
    successCode(res, data, "Trả dữ liệu thành công");
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const getFood = async (req, res) => {
  try {
    let id = req.params.id;
    let data = await model.food.findOne({
      where: {
        food_id: id,
      },
      include: ["type", "sub_foods"],
    });
    successCode(res, data, "Trả dữ liệu thành công");
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

const orderFood = async (req, res) => {
  try {
    model.order.create({ ...req.body, code: "" });
    successCode(res, req.body, "Tạo mới thành công");
  } catch (error) {
    errorCode(res, "Lỗi BE");
  }
};

module.exports = {
  getAllFood,
  getFood,
  orderFood,
};
