const {
  getUser,
  getAllUser,
  createUser,
  updateUser,
  searchUser,
  deleteUser,
} = require("../controllers/userControllers");
const express = require("express");
const userRoute = express.Router();

userRoute.get("/getUser/:id", getUser);

userRoute.get("/getAllUser", getAllUser);

userRoute.get("/searchUser", searchUser);

userRoute.post("/createUser", createUser);

userRoute.put("/updateUser/:id", updateUser);

userRoute.delete("/deleteUser/:id", deleteUser);

module.exports = userRoute;
