const express = require("express");
const rootRoute = express.Router();

const userRoute = require("./userRoute");
const foodRoute = require("./foodRoute");
const restaurantRoute = require("./restaurantRoute");

rootRoute.use("/user", userRoute);
rootRoute.use("/food", foodRoute);
rootRoute.use("/restaurant", restaurantRoute);

module.exports = rootRoute;
