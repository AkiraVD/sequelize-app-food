const express = require("express");
const foodRoute = express.Router();

const {
  getAllFood,
  getFood,
  orderFood,
} = require("../controllers/foodControllers");

foodRoute.get("/getAllFood", getAllFood);

foodRoute.get("/getFood/:id", getFood);

foodRoute.post("/orderFood/", orderFood);

module.exports = foodRoute;
