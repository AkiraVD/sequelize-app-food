const express = require("express");
const restaurantRoute = express.Router();
const {
  getRestaurant,
  likeRestaurant,
  unlikeRestaurant,
  rateRestaurant,
} = require("../controllers/restaurantControllers");

restaurantRoute.get("/getRestaurant/:id", getRestaurant);

restaurantRoute.post("/likeRestaurant/", likeRestaurant);

restaurantRoute.delete("/unlikeRestaurant/", unlikeRestaurant);

restaurantRoute.post("/rateRestaurant/", rateRestaurant);

module.exports = restaurantRoute;
